# CSE 474
# Programming Assignment 1
#
# Group #17:
#   Amal Khandelwal
#   Rahul Tripathi
#   Yoseph Habtewold
#

import numpy as np
from scipy.optimize import minimize
from scipy.io import loadmat
from math import sqrt
import random
from scipy.stats import logistic
# import pickle as pickle

def initializeWeights(n_in,n_out):
    epsilon = sqrt(6) / sqrt(n_in + n_out + 1);
    W = (np.random.rand(n_out, n_in + 1)*2* epsilon) - epsilon;
    return W


def sigmoid(z):
    return logistic.cdf(z)


def preprocess():
    mat = loadmat('../original/mnist_all.mat')

    def getData(key):
        data = []
        labels = []

        for i in range(10):
            data.extend(mat[key + str(i)])
            labels.extend(np.repeat([i], mat[key + str(i)].shape[0]))

        return np.array(data), np.array(labels)

    def get_redundant_features(A):
        cols_to_del = []

        for j in range(A.shape[1]):
            if len(set(A[:,j])) <= 1:
                cols_to_del.append(j)

        return cols_to_del

    # stack
    all_data, all_label = getData('train')
    test_data, test_label = getData('test')

    # select features
    cols_to_del = get_redundant_features(all_data)
    all_data = np.delete(all_data, cols_to_del, 1)
    test_data = np.delete(test_data, cols_to_del, 1)

    # normalize
    all_data = np.divide(all_data, 255.0)
    test_data = np.divide(test_data, 255.0)

    # split into train and validation
    split_size = 10000
    aperm = np.random.permutation(range(all_data.shape[0]))
    validation_data = all_data[aperm[:split_size],:]
    train_data = all_data[aperm[split_size:],:]
    validation_label = all_label[aperm[:split_size]]
    train_label = all_label[aperm[split_size:]]

    return train_data, train_label, validation_data, validation_label, test_data, test_label



def nnObjFunction(params, *args):
    n_input, n_hidden, n_class, training_data, training_label, lambdaval = args

    w1 = params[0:n_hidden * (n_input + 1)].reshape((n_hidden, (n_input + 1)))
    w2 = params[(n_hidden * (n_input + 1)):].reshape((n_class, (n_hidden + 1)))
    obj_val = 0

    n_data = training_data.shape[0]

    # Feedforward
    z = np.zeros((n_data, n_hidden + 1))
    z[:,-1] = 1  # add bias

    o = np.zeros((n_data, n_class))
    y = np.zeros((n_data, n_class))

    training_data = np.c_[training_data, np.ones(n_data)]   # add bias

    z = sigmoid(np.dot(training_data, w1.T))
    z = np.c_[z, np.ones(z.shape[0])]

    o = sigmoid(np.dot(z, w2.T))

    for p in range(n_data):
        y[p][training_label[p]] = 1

    # Backpropagation
    J = np.power(y - o, 2).sum() / (2 * n_data)

    delta_out = ((y - o) * (1 - o) * o)

    grad_w2 = -1 * np.dot(delta_out.T, z)

    grad_w1 = -1 * np.dot((((1 - z) * z) * np.dot(delta_out, w2)).T, training_data)
    grad_w1 = np.delete(grad_w1, n_hidden, axis=0)

    # Regularization
    obj_val = J + (lambdaval / (2 * n_data)) * (np.power(w1, 2).sum() + np.power(w2, 2).sum())

    grad_w2 = (grad_w2 + lambdaval * w2) / n_data
    grad_w1 = (grad_w1 + lambdaval * w1) / n_data

    obj_grad = np.concatenate((grad_w1.flatten(), grad_w2.flatten()),0)

    print(obj_val)

    return (obj_val, obj_grad)



def nnPredict(w1, w2, data):
    n_data = data.shape[0]

    labels = np.zeros(n_data)
    data = np.c_[data, np.ones(n_data)]

    z = np.zeros((n_data, w2.shape[1]))
    z[:,-1] = 1  # add bias

    o = np.zeros((n_data, n_class))

    z = sigmoid(np.dot(data, w1.T))
    z = np.c_[z, np.ones(z.shape[0])]

    o = sigmoid(np.dot(z, w2.T))

    for i in range(n_data):
        labels[i] = np.argmax(o[i])

    return labels



"""**************Neural Network Script Starts here********************************"""

train_data, train_label, validation_data,validation_label, test_data, test_label = preprocess();


# Train Neural Network

# set the number of nodes in input unit (not including bias unit)
n_input = train_data.shape[1];

# set the number of nodes in hidden unit (not including bias unit)
n_hidden = 30;

# set the number of nodes in output unit
n_class = 10;

# initialize the weights into some random matrices
initial_w1 = initializeWeights(n_input, n_hidden);
initial_w2 = initializeWeights(n_hidden, n_class);

# unroll 2 weight matrices into single column vector
initialWeights = np.concatenate((initial_w1.flatten(), initial_w2.flatten()), 0)

# set the regularization hyper-parameter
lambdaval = 0.1;


args = (n_input, n_hidden, n_class, train_data, train_label, lambdaval)

# Train Neural Network using minimize from scipy.optimize module

opts = {'maxiter' : 50, 'disp': True}    # Preferred value.


nn_params = minimize(nnObjFunction, initialWeights, jac=True, args=args, method='CG', options=opts)

#Reshape nnParams from 1D vector into w1 and w2 matrices
w1 = nn_params.x[0:n_hidden * (n_input + 1)].reshape((n_hidden, (n_input + 1)))
w2 = nn_params.x[(n_hidden * (n_input + 1)):].reshape((n_class, (n_hidden + 1)))


#Test the computed parameters

predicted_label = nnPredict(w1,w2,train_data)

#find the accuracy on Training Dataset

print('\n Training set Accuracy:' + str(100*np.mean((predicted_label == train_label).astype(float))) + '%')

predicted_label = nnPredict(w1,w2,validation_data)

#find the accuracy on Validation Dataset

print('\n Validation set Accuracy:' + str(100*np.mean((predicted_label == validation_label).astype(float))) + '%')


predicted_label = nnPredict(w1,w2,test_data)

#find the accuracy on Validation Dataset

print('\n Test set Accuracy:' + str(100*np.mean((predicted_label == test_label).astype(float))) + '%')

# pickle.dump([lambdaval,n_hidden,w1,w2],open('f.pickle','wb'))

