# Handwritten Digit Recognition  
- /nn contains the script for classification using Neural Networks.
- /lr_svn contains the script for classification using Logistic Regression and Support Vector Machine.
- Same dataset is used for all three classifications.
- /lr_svn/report.pdf contains the report.

## Neural Networks Classification
- mnist all.mat: original dataset from MNIST. In this file, there are 10 matrices for testing set and 10 matrices for training set, which corresponding to 10 digits.   
- The MNIST dataset [1] consists of a training set of 60000 examples and test set of 10000 examples. All
digits have been size-normalized and centered in a fixed image of 28×28 size. In original dataset, each pixel
in the image is represented by an integer between 0 and 255, where 0 is black, 255 is white and anything
between represents different shade of gray.
In many research papers, the official training set of 60000 examples is divided into an actual training set of
50000 examples and validation set of 10000 examples.
- nnScript.py is the main script:
  -  preprocess(): performs some preprocess tasks, and output the preprocessed train, validation and
test data with their corresponding labels.
  - sigmoid(): compute sigmoid function. The input can be a scalar value, a vector or a matrix.
  -  nnObjFunction(): compute the error function of Neural Network.
  -  nnPredict(): predicts the label of data given the parameters of Neural Network.
  -   initializeWeights(): return the random weights for Neural Network given the number of unit in
the input layer and output layer.

- Neural Network Representation:
  - The first layer comprises of (d + 1) units, each represents a feature of image (there is one extra unit
representing the bias)
  - The second layer in neural network is called the hidden units. In this document, we denote m + 1
as the number of hidden units in hidden layer. There is an additional bias node at the hidden layer
as well. Hidden units can be considered as the learned features extracted from the original data set.
Since number of hidden units will represent the dimension of learned features in neural network, it’s
our choice to choose an appropriate number of hidden units. Too many hidden units may lead to the
slow training phase while too few hidden units may cause the the under-fitting problem.
  - The third layer is also called the output layer. The value of l th unit in the output layer represents
the probability of a certain hand-written image belongs to digit l. Since we have 10 possible digits,
there are 10 units in the output layer. In this document, we denote k as the number of output units
in output layer.
  - The parameters in Neural Network model are the weights associated with the hidden layer units and the
output layers units. In our standard Neural Network with 3 layers (input, hidden, output), in order to
represent the model parameters, we use 2 matrices:
    - W (1) ∈ R m×(d+1) is the weight matrix of connections from input layer to hidden layer. Each row in
this matrix corresponds to the weight vector at each hidden layer unit.
    - W (2) ∈ R k×(m+1) is the weight matrix of connections from hidden layer to output layer. Each row in
this matrix corresponds to the weight vector at each output layer unit.

  - We also further assume that there are n training samples when performing learning task of Neural Network.
  - sigmoid: compute sigmoid function. The input can be a scalar value, a vector or a matrix.
  - nnObjFunction: computes the objective function of Neural Network with regularization and the gradient
of objective function.
  - nnPredict: predicts the label of data given the parameters of Neural Network.
 ## Logistic Regression
- Since there are 10 possible values that a digit can take we are using 10 binary classifiers (*one vs all strategy*). 
- blrObjFunction(): The function contains three parameters: 
  - X is a data matrix where each row contains a feature vector in original coordinate (X ∈ R N×D).
  - w k is a column vector representing the parameters of Logistic Regression. Size of w k is (D + 1) × 1.
  - y k is a column vector representing the labels of corresponding feature vectors in data matrix X. Each
entry in this vector is either 1 or 0 to represent whether the feature vector belongs to a class C k or not
(k = 0,1,··· ,K − 1). Size of y k is N × 1 where N is the number of rows of X.
  - The function returns a scalar value error, and error_grad a column vector of size (D + 1) X 1 representing the gradient of error function obtained by using the equation:
    - ∇E(w) = (1/N) × *Summation(n = 1 to N) of* { (θ n − y n) × Xn } 
- For prediction using Logistic Regression, given 10 weights vector of 10 classes, we need to classify a feature
vector into a certain class. In order to do so, given a feature vector x, we need to compute the posterior
probability P(y = C k |x) and the decision rule is to assign x to class C k that maximizes P(y = C k |x).
- blrPredict(): 
    -  Returns the predicted label for each feature vector.
    -  The function takes two inputs. X is also a data matrix where each row contains a feature vector
in original coordinate (not including the bias 1 at the beginning of vector). X has a size of N × D.
  - W is a matrix where each column is a weight vector (w k ) of classifier for digit k. Concretely, W has
size (D + 1) × K where K = 10 is the number of classifiers.
  - The output of function blrPredict() is a column vector label which has size N × 1.

## Support Vector Machines

- Used the support vector machine tool in sklearn to perform classification. More details can be found at  http://scikit-learn.
org/stable/modules/generated/sklearn.svm.SVC.html
- Following parameters are used:
  -  Linear kernel.
  -  Radial basis function with value of gamma setting to 1.